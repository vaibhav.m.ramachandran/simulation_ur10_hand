# Simulation_ur10_hand

This simulation is for internal use and is currently under development.
the purpose of this simulation is to generate data for training a model

#Tested Environment#
Ubuntu LTS20.04; ros noetic

#Procedure to get started#
1. git clone <ssh or https> 
2. catkin build #here you check for missing dependencies and install 
3. source devel/setup.bash
3.1 roslaunch ur_gazebo ur10.launch
In a new terminal 
4. source devel/setup.bash
4.1 roslaunch ur_simulation_main ur10_planning_execution.launch 

Planning and execution can be tested using the preset arm group states
"home" and "start" as shown in the attached video in the resources folder 
[ActivityVid](resources/act.mp4)

#RViZ pick and place tutorial#
3. source devel/setup.bash
3.1 roslaunch ur10_hand_moveit_config demo.launch
In a new terminal 
4. source devel/setup.bash
4.1 rosrun ur_simulation_main pick_place_tutorial
